<?php
  if(!defined('DB_TYPE'))
  {
    /** Database settings
    *   DB_TYPE values : mysql or pgsql respectivly for MySQL and PostgreSQL
    *   DB_NAME : the database name
    *   DB_USER : the sql database user
    *   DB_PASSWORD : the password required for DB_USER to authenticate
    *   DB_HOST : the host (ip or hostname) where the database is stored
    */
    define('DB_TYPE', 'pgsql');
    define('DB_NAME', 'framalang');
    define('DB_USER', 'framalang');
    define('DB_PASSWORD', 'framapass');
    define('DB_HOST', 'localhost');

    /** Other settings
    *   SALT : a string that will be inserted the users' password
    *   PAGE_SIZE : the number of results per page
    *   LIST_PAGE : the fullname of the php file where results are displayed
    */
    define('APP_URLBASE','http://marisson/framalang');
    define('APP_NAME', 'Framalang - Traductions');
    define('SALT', 'pouet');
    define('PAGE_SIZE','50');
    define('LIST_PAGE','trads.php');
  }
?>
