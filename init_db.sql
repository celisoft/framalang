CREATE TABLE STATUS (
  ID_Status smallint NOT NULL PRIMARY KEY,
  Name_Status varchar(30) NOT NULL,
  Percent_Status smallint NOT NULL DEFAULT '0'
);

INSERT INTO STATUS(ID_Status, Name_Status, Percent_Status) VALUES (0, 'En cours', 1);
INSERT INTO STATUS(ID_Status, Name_Status, Percent_Status) VALUES (1, 'En relecture', 2);
INSERT INTO STATUS(ID_Status, Name_Status, Percent_Status) VALUES (2, 'En attente de publication', 3);
INSERT INTO STATUS(ID_Status, Name_Status, Percent_Status) VALUES (3, 'Publiée', 4);

CREATE TABLE TRANSLATOR (
  ID_User serial PRIMARY KEY,
  Pseudo_User varchar(50) NOT NULL,
  Pass_User varchar(256) NOT NULL,
  Mail_User varchar(150) NOT NULL,
  IS_Admin boolean DEFAULT FALSE,
);

CREATE TABLE TRANSLATION (
  ID_Trans serial PRIMARY KEY,
  Title_Trans varchar(127) NOT NULL,
  UrlPad_Trans varchar(255) DEFAULT NULL,
  UrlPub_Trans varchar(255) DEFAULT NULL,
  DateCre_Trans date NOT NULL,
  DateUpdate_Trans date NOT NULL,
  DateEnd_Trans date DEFAULT NULL,
  IS_Prior boolean DEFAULT FALSE,
  ID_Status smallint REFERENCES STATUS(ID_Status)
);
