<?php
    require_once dirname(__FILE__).'/settings.php';

    require_once dirname(__FILE__).'/class/ui/navbar.php';
    require_once dirname(__FILE__).'/class/ui/flash.php';
    require_once dirname(__FILE__).'/class/ui/global_translation_view.php';

    session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <title>
      <?php echo APP_NAME; ?>
    </title>
    <meta name="charset" content= "utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="includes/fwk/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="includes/css/framalang.css"/>
  </head>
  <body>
    <?php
      //Set the current translator
      $translator = null;

      if(isset($_SESSION['USER']))
      {
        $translator = $_SESSION['USER'];
      }

      //Displays the navbar with only needed content (modals included)
      $navbar = new navbar(APP_NAME, $translator);
      $navbar->display();
    ?>
    <div id="container">
      <?php
        //Displays the flash if exists
        if(isset($_SESSION['FLASH']))
        {
          $_SESSION['FLASH']->display();
          unset($_SESSION['FLASH']);
        }

        $view = new global_translation_view(false);
        $view->display();
        unset($view);
      ?>
    </div>
    <div id="footer">
      <div class="container">
         <p class="text-muted credit">Réalisé par Céline Libéral pour le Framalang (2013-2014).</p>
      </div>
    </div>

    <script type="text/javascript" src="./includes/fwk/jquery/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="./includes/fwk/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
                $('.dropdown-toggle').dropdown();
        });
    </script>
  </body>
</html>
