framalang
=========

Project description
-------------------
Framalang translation management tool

Setup
-----

**Setup database**

This project has only be tested under MySQL 5 and PostgreSQL 9.3. Maybe it is possible to use sqlite but there are no guarantee.
Steps are quite easy to initailize the database, you just have to use the *init_db.sql* script.

**Configure settings.php**

The *settings.php* file contains 10 parameters that must be filled before deployment:
- "DB_TYPE" 	   *mysql* or *pgsql* respectivly for MySQL and PostgreSQL
- "DB_NAME"	   the database name
- "DB_USER"	   the sql database user
- "DB_PASSWORD"	   the password required for DB_USER to authenticate
- "DB_HOST"	   the host (ip or hostname) where the database is stored
- "APP_URLBASE" the full url where is deployed the app (for me it's "http://marisson/framalang")
- "APP_NAME"  the name displayed in the navbar brand and in the page title
- "SALT"	   a string that will be inserted before the users' password
- "PAGE_SIZE"	   the number of results per page
- "LIST_PAGE"	   the fullname of the php file where results are displayed

To have a good "SALT" value, you can use *uuidgen* in a terminal.

**Initialize admin user**

In a terminal, type in the following command:
<!-- language: lang-sh -->
     echo -n "SALTPASSWORD" | md5sum

The printed out string will be the admin user password stored in database. If your salt value is "SALT" and you want your admin user to have a password "PASSWORD", the returned value will be "340696158bcb72fb8642f37f889b3568".

Then, you have just to connect to your database and enter the following SQL request:
<!-- language: lang-sql -->
     INSERT INTO TRANSLATOR(Pseudo_User, Pass_User, Mail_User, IS_Admin) VALUES ('admin_user', '340696158bcb72fb8642f37f889b3568', 'celine.liberal@foobar.org', TRUE);

Other users can be managed through the HMI.
