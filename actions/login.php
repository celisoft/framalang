<?php
  require_once dirname(__FILE__).'/../settings.php';

  require_once dirname(__FILE__).'/../class/translator.php';
  require_once dirname(__FILE__).'/../class/ui/flash.php';

  session_start();

  $url = APP_URLBASE;
  $page = LIST_PAGE;

  if(isset($_GET['modal_login_zt_pseudo']) && isset($_GET['modal_login_zt_password']))
  {
    $name = $_GET['modal_login_zt_pseudo'];
    $pass = $_GET['modal_login_zt_password'];

    $user = new Translator($name, $pass);
    $user = $user->load();

  	if($user === null)
  	{
      //Wrong password
      session_destroy();
      header("Location: $url/$page");
      exit;
    }
  	else
  	{
      session_regenerate_id();
      $_SESSION['USER'] = $user;
      $_SESSION['FLASH'] = new flash("Identification réussie","Vous êtes maintenant identifié(e) en tant que ".$user->getPseudo().".", false);

      header("Location: $url/$page");
  	}
  }
  else
  {
    //Bad guy !!
    if(isset($_SESSION['USER']))
    {
      $_SESSION['FLASH'] = new flash("Opération non permise","Cette opération est interdite dans ce contexte.", true);
    }

    header("Location: $url/$page");
    exit;
  }
?>
