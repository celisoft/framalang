<?php
    session_start();
    session_destroy();

    require_once dirname(__FILE__).'/../settings.php';

    $url = APP_URLBASE;
    $page = LIST_PAGE;

    header("Location: $url/$page");
    exit;
?>
