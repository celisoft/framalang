<?php
  require_once dirname(__FILE__).'/../settings.php';

  require_once dirname(__FILE__).'/../class/translator.php';
  require_once dirname(__FILE__).'/../class/ui/flash.php';

  session_start();

  $url = APP_URLBASE;
  $page = LIST_PAGE;

  if(isset($_SESSION['USER']) && isset($_GET['modal_edit_profile_zt_pass']) && isset($_GET['modal_edit_profile_zt_mail']))
  {
    $translator = $_SESSION['USER'];

    $translator->setPassword($_GET['modal_edit_profile_zt_pass']);
    $translator->setMail($_GET['modal_edit_profile_zt_mail']);

    $save_success = $translator->save(true);

    if($save_success)
    {
      $_SESSION['USER'] = $translator;
      $_SESSION['FLASH'] = new flash("Mise à jour terminée","Votre profil a été mis à jour.", false);

      header("Location: $url/$page");
      exit;
    }
    else
    {
      //User not saved
      $_SESSION['FLASH'] = new flash("Echec","La mise à jour de votre profil a échoué.", true);
      header("Location: $url/$page");
      exit;
    }
  }
  else
  {
    //Bad guy !!
    if(isset($_SESSION['USER']))
    {
      $_SESSION['FLASH'] = new flash("Opération non permise","Cette opération est interdite dans ce contexte.", true);
    }

    header("Location: $url/$page");
    exit;
  }
?>
