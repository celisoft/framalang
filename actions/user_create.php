<?php
  require_once dirname(__FILE__).'/../settings.php';

  require_once dirname(__FILE__).'/../class/translator.php';
  require_once dirname(__FILE__).'/../class/ui/flash.php';

  session_start();

  $url = APP_URLBASE;
  $page = LIST_PAGE;

  if(isset($_SESSION['USER']))
  {
    if($_SESSION['USER']->isAdmin())
    {
      if(isset($_GET['modal_add_user_zt_pseudo']) && isset($_GET['modal_add_user_zt_mail']) && isset($_GET['modal_add_user_zt_pass']))
      {
        $newUser = new Translator($_GET['modal_add_user_zt_pseudo'], $_GET['modal_add_user_zt_pass']);
        $newUser->setMail($_GET['modal_add_user_zt_mail']);

        if(isset($_GET['modal_add_user_chk_admin']))
        {
          $newUser->setAsAdmin();
        }

        $save_success = $newUser->save(false);

        if($save_success)
        {
          $_SESSION['FLASH'] = new flash("Succés", "L'utilisateur ".$newUser->getPseudo()." a été créé.", false);

          header("Location: $url/$page");
          exit;
        }
        else
        {
          //User not saved
          $_SESSION['FLASH'] = new flash("Echec","L'utilisateur ".$newUser->getPseudo()." n'a pas été créé.", true);
          header("Location: $url/$page");
          exit;
        }
      }
    }
    else
    {
      //Bad guy !!
      if(isset($_SESSION['USER']))
      {
        $_SESSION['FLASH'] = new flash("Opération non permise","Cette opération est interdite dans ce contexte.", true);
      }
    }
  }
  else
  {
    header("Location: $url/$page");
    exit;
  }
?>
