<?php
    session_start();

    require_once dirname(__FILE__).'/../settings.php';
    require_once dirname(__FILE__).'/../includes/php/db_connection_class.php';
    
    if(isset($_SESSION['USER']) && isset($_GET['zt_trad_name']) && isset($_GET['zt_trad_padurl']) && isset($_GET['zt_trad_puburl']) && isset($_GET['h_id']))
    {
        $lCon = new DatabaseConnection(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASSWORD);
        $lConPDO = $lCon->openConnection();

        $sqlString = "UPDATE TRANSLATION SET Title_Trans='".$_GET['zt_trad_name']."', UrlPad_Trans='".$_GET['zt_trad_padurl']."', UrlPub_Trans='".$_GET['zt_trad_puburl']."', ";
        
        if(isset($_GET['chk_trad_prio']))
        {
            $sqlString = $sqlString."ID_Prio='1'";
        }
        else
        {
            $sqlString = $sqlString."ID_Prio='0'";
        }
        
        if(isset($_GET['zl_etat']))
        {
            if($_GET['zl_etat'] == 3)
            {
                $sqlString = $sqlString.", ID_Status='".$_GET['zl_etat']."', DateEnd_Trans='".date("Y-m-d")."'";
            }
            else
            {
                $sqlString = $sqlString.", ID_Status='".$_GET['zl_etat']."'";
            }
        }

        $sqlString = $sqlString." WHERE ID_Trans='".$_GET['h_id']."';";
        
        $sth = $lConPDO->exec($sqlString);

        $lCon->closeConnection();
    }

    $host  = $_SERVER['HTTP_HOST'];
    $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $page = '../'.LIST_PAGE;

    header("Location: http://$host$uri/$page");
    exit;
?>