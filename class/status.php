<?php
  class status
  {
    private $id;
    private $name;
    private $percentage;

    public function __construct($pIdStatus)
    {
      $this->id = $pIdStatus;
      $this->updateInfo();
    }

    private function updateInfo()
    {
      $sql = "SELECT Name_Status, Percent_Status FROM STATUS WHERE ID_Status=".$this->id.";";

      $db_conn = new DatabaseConnection();
      $row = $db_conn->doSelect($sql, FALSE);
      $db_conn->closeConnection();

      $this->name = $row['name_status'];
      $this->percentage = $row['percent_status'];
    }

    public function getIdStatus()
    {
      return $this->id;
    }

    public function getStatusName()
    {
      return $this->name;
    }

    public function getStatusPercentage()
    {
      return $this->percentage;
    }
  }
?>
