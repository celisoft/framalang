<?php
	require_once dirname(__FILE__).'/db_connection.php';
	require_once dirname(__FILE__).'/../settings.php';

	class Translator {
		private $id;
		private $pseudo;
		private $password;
		private $mail;
		private $is_admin;

		/**
		*	Constructor used to create a user with just a pseudo and a password.
		* Returns the user object if given parameters are given else return FALSE
		*/
		public function __construct($pPseudo, $pPassword) {
			if(isset($pPseudo) && isset($pPassword))
			{
				$this->id = null;
				$this->pseudo = $pPseudo;
				$this->password = md5(SALT.$pPassword);
				$this->mail = null;
				$this->is_admin = false;

				return $this;
			}
			else
			{
				return false;
			}
		}

		/**
	  *	Used to load a user
		*	Returns the user if it exists, returns null instead
	  **/
	  public function load()
	  {
			$sql = "SELECT id_user, count(ID_User) as nb_user, mail_user, is_admin  FROM TRANSLATOR WHERE Pseudo_User='".$this->pseudo."' AND Pass_User='".$this->password."' GROUP BY ID_User;";

			$db_conn = new DatabaseConnection();
			$row = $db_conn->doSelect($sql, FALSE);
			$db_conn->closeConnection();

	    if($row['nb_user'] == 1)
	    {
				$this->id = $row['id_user'];
				$this->mail = $row['mail_user'];
				$this->is_admin = $row['is_admin'];

	      return $this;
	    }

			return null;
	  }

		/**
		* Save the user into database
		*/
		public function save($isUpdate)
		{
			$admin_sql_boolean = 'FALSE';
			if($this->is_admin)
			{
				$admin_sql_boolean = 'TRUE';
			}
			if($isUpdate)
			{
				//Update the user
				$sql = "UPDATE TRANSLATOR SET pass_user='".$this->password."', mail_user='".$this->mail."', is_admin=".$admin_sql_boolean." WHERE id_user='".$this->id."';";
			}
			else
			{
				//Insert the user
				$sql = "INSERT INTO TRANSLATOR (pseudo_user, pass_user, mail_user, is_admin) VALUES ('".$this->pseudo."','".$this->password."','".$this->mail."',".$admin_sql_boolean.");";
			}

			$db_conn = new DatabaseConnection();
			$result = $db_conn->doUpdateOrInsert($sql);
			$db_conn->closeConnection();

			if($result == 1)
			{
				return true;
			}

			return false;
		}

		/**
		*	Return the translator pseudonym
		**/
		public function getPseudo()
		{
			return $this->pseudo;
		}

		/**
		*	Set the password with SALT inclusion & MD5 hashing
		*/
		public function setPassword($pPassword)
		{
			$this->password = md5(SALT.$pPassword);
		}

		/**
		*	Return the translator mail address
		*/
		public function getMail()
		{
			return $this->mail;
		}

		/**
		*	Set a new address mail for the translator
		*/
		public function setMail($pMail)
		{
			$this->mail = $pMail;
		}

		/**
		*	Return the boolean value indicating if the translator is an admin
		* TRUE = the translator is an admin
		* FALSE = the translator is a basic user
		*/
		public function isAdmin()
		{
			return $this->is_admin;
		}

		/**
		*	Give admin privileges to the user
		*/
		public function setAsAdmin()
		{
			$this->is_admin = true;
		}

		public function __destruct()
		{
			unset($this);
		}
	}
?>
