<?php
  class ModalAddUser
  {
    private $modal_id;
    private $title;
    private $php_destination;

    /**
    * A modal is defined by an ID, a title and a php destination
    */
    public function __construct($pId, $pTitle, $pDestination)
    {
      $this->modal_id = $pId;
      $this->title = $pTitle;
      $this->php_destination = $pDestination;
    }

    public function generate()
    {
      echo '<div class="modal fade" id="'.$this->modal_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
      echo '<div class="modal-dialog">';
        echo '<div class="modal-content">';
          echo '<form id="modal-form" action="'.$this->php_destination.'" method="get" data-remote="true" class="form-horizontal" role="form">';
            echo '<div class="modal-header">';
                echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
                echo '<h4 class="modal-title" id="myModalLabel">'.$this->title.'</h4>';
            echo '</div>';
            echo '<div class="modal-body">';
                echo '<div class="form-group">';
                    echo '<label for="modal_add_user_zt_pseudo" class="col-sm-3 control-label">Pseudonyme</label>';
                    echo '<div class="col-sm-9">';
                      echo '<input type="text" id="modal_add_user_zt_pseudo" name="modal_add_user_zt_pseudo" maxlength="50" class="form-control" placeholder="Pseudonyme" required>';
                    echo '</div>';
                echo '</div>';
                echo '<div class="form-group">';
                    echo '<label for="modal_add_user_zt_mail" class="col-sm-3 control-label">Adresse mail</label>';
                    echo '<div class="col-sm-9">';
                      echo '<input type="email" id="modal_add_user_zt_mail" name="modal_add_user_zt_mail" maxlength="150" class="form-control" placeholder="Adresse mail" required>';
                    echo '</div>';
                echo '</div>';
                echo '<div class="form-group">';
                    echo '<label for="modal_add_user_zt_pass" class="col-sm-3 control-label">Mot de passe</label>';
                    echo '<div class="col-sm-9">';
                      echo '<input type="password" id="modal_add_user_zt_pass" name="modal_add_user_zt_pass" class="form-control" required>';
                    echo '</div>';
                echo '</div>';
                echo '<div class="checkbox">';
                  echo '<div class="col-sm-offset-2 col-sm-9">';
                    echo '<label>';
                      echo '<input type="checkbox" name="modal_add_user_chk_admin" value="prio">Cocher cette case si l\'utilisateur est un administrateur';
                    echo '</label>';
                  echo '</div>';
                echo '</div>';
            echo '</div>';
            echo '<div class="modal-footer">';
                echo '<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>';
                echo '<input type="submit" class="btn btn-primary" value="Mettre à jour" />';
            echo '</div>';
          echo '</form>';
        echo '</div>';
      echo '</div>';
      echo '</div>';
    }

    public function __destruct()
    {
      unset($this);
    }
  }
?>
