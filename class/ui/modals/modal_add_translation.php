<?php
  class ModalAddTranslation
  {
    private $modal_id;
    private $title;
    private $php_destination;

    /**
    * A modal is defined by an ID, a title and a php destination
    */
    public function __construct($pId, $pTitle, $pDestination)
    {
      $this->modal_id = $pId;
      $this->title = $pTitle;
      $this->php_destination = $pDestination;
    }

    public function generate()
    {
      echo '<div class="modal fade" id="'.$this->modal_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
        echo '<div class="modal-dialog">';
          echo '<div class="modal-content">';
            echo '<form id="modal-form" action="'.$this->php_destination.'" method="get" data-remote="true" class="form-horizontal" role="form">';
              echo '<div class="modal-header">';
                echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
                echo '<h4 class="modal-title" id="myModalLabel">'.$this->title.'</h4>';
              echo '</div>';
              echo '<div class="modal-body">';
                echo '<div class="form-group">';
                  echo '<label for="modal_add_trans_zt_trad_name" class="col-sm-3 control-label">Titre</label>';
                  echo '<div class="col-sm-9">';
                    echo '<input type="text" id="modal_add_trans_zt_trad_name" name="modal_add_trans_zt_trad_name" maxlength="127" class="form-control" placeholder="Titre de la traduction">';
                  echo '</div>';
                echo '</div>';
                echo '<div class="form-group">';
                  echo '<label for="modal_add_trans_zt_trad_padurl" class="col-sm-3 control-label">Url du pad</label>';
                  echo '<div class="col-sm-9">';
                    echo '<input type="text" id="modal_add_trans_zt_trad_padurl" name="modal_add_trans_zt_trad_padurl" class="form-control" placeholder="URL du pad de traduction (avec http(s)://)">';
                  echo '</div>';
                echo '</div>';
                echo '<div class="checkbox">';
                  echo '<div class="col-sm-offset-2 col-sm-9">';
                    echo '<label>';
                    echo '<input type="checkbox" name="modal_add_trans_chk_trad_prio" value="prio">Cocher cette case si la traduction est prioritaire';
                    echo '</label>';
                  echo '</div>';
                echo '</div>';
              echo '</div>';
              echo '<div class="modal-footer">';
                echo '<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>';
                echo '<input type="submit" class="btn btn-primary" value="Ajouter" />';
              echo '</div>';
            echo '</form>';
          echo '</div>';
        echo '</div>';
      echo '</div>';
    }

    public function __destruct()
    {
      unset($this);
    }
  }
?>
