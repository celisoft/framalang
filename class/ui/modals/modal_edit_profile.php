<?php
  require_once dirname(__FILE__).'/../../translator.php';

  class ModalEditProfile
  {
    private $modal_id;
    private $title;
    private $user;
    private $php_destination;

    /**
    * A modal used to edit user profile
    */
    public function __construct($pId, $pTitle, $pUser, $pDestination)
    {
      $this->modal_id = $pId;
      $this->title = $pTitle;
      $this->user = $pUser;
      $this->php_destination = $pDestination;
    }

    /**
    * A modal auto-filled with current user data in order to edit them
    */
    public function generate()
    {
      echo '<div class="modal fade" id="'.$this->modal_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
      echo '<div class="modal-dialog">';
        echo '<div class="modal-content">';
          echo '<form id="modal-form" action="'.$this->php_destination.'" method="get" data-remote="true" class="form-horizontal" role="form">';
            echo '<div class="modal-header">';
                echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
                echo '<h4 class="modal-title" id="myModalLabel">'.$this->title.'</h4>';
            echo '</div>';
            echo '<div class="modal-body">';
                echo '<div class="form-group">';
                    echo '<label for="modal_edit_profile_zt_pseudo" class="col-sm-3 control-label">Pseudonyme</label>';
                    echo '<div class="col-sm-9">';
                        echo '<input type="text" id="modal_edit_profile_zt_pseudo" name="modal_edit_profile_zt_pseudo" maxlength="50" class="form-control" value="'.$this->user->getPseudo().'" disabled>';
                    echo '</div>';
                echo '</div>';
                echo '<div class="form-group">';
                    echo '<label for="modal_edit_profile_zt_pass" class="col-sm-3 control-label">Mot de passe</label>';
                    echo '<div class="col-sm-9">';
                        echo '<input type="password" id="modal_edit_profile_zt_pass" name="modal_edit_profile_zt_pass" class="form-control" required>';
                    echo '</div>';
                echo '</div>';
                echo '<div class="form-group">';
                    echo '<label for="modal_edit_profile_zt_mail" class="col-sm-3 control-label">Adresse mail</label>';
                    echo '<div class="col-sm-9">';
                        echo '<input type="email" id="modal_edit_profile_zt_mail" name="modal_edit_profile_zt_mail" maxlength="150" class="form-control" value="'.$this->user->getMail().'" required>';
                    echo '</div>';
                echo '</div>';
            echo '</div>';
            echo '<div class="modal-footer">';
                echo '<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>';
                echo '<input type="submit" class="btn btn-primary" value="Mettre à jour" />';
            echo '</div>';
          echo '</form>';
        echo '</div>';
      echo '</div>';
      echo '</div>';
    }

    public function __destruct()
    {
      unset($this);
    }
  }
?>
