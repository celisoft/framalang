<?php
  require_once dirname(__FILE__).'/../translator.php';

  require_once dirname(__FILE__).'/modals/modal_login.php';

  require_once dirname(__FILE__).'/modals/modal_edit_profile.php';
  require_once dirname(__FILE__).'/modals/modal_add_translation.php';

  require_once dirname(__FILE__).'/modals/modal_add_user.php';

  class navbar
  {
    private $title;
    private $user;

    /**
    *	Default constructor.
    */
    public function __construct($pTitle, $pUser = null)
    {
      $this->title = $pTitle;
      $this->user = $pUser;
    }

    /**
    *	Render the HTML code with a Bootstrap navbar
    */
    public function display()
    {
      $this->generateModals();

      echo '<div class="navbar navbar-inverse navbar-static-top" role="navigation">';
      echo '<div class="container-fluid">';

        echo '<div class="navbar-header">';
          echo '<a class="navbar-brand">'.$this->title.'</a>';
        echo '</div>';

        echo '<div class="collapse navbar-collapse">';
        echo '<ul class="nav navbar-nav navbar-right">';

          if(!isset($this->user))
          {
            echo '<li><a href="#" data-toggle="modal" data-target="#modal_login">S\'identifier</a></li>';
          }
          else
          {
            if($this->user->isAdmin())
            {
              echo '<li class="dropdown">';
                echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown">Administration<span class="caret"></span></a>';
                echo '<ul class="dropdown-menu" role="menu">';
                  echo '<li><a href="#" data-toggle="modal" data-target="#modal_add_user">Ajouter un utilisateur</a></li>';
                  echo '<li><a class="disabled" href="#" data-toggle="modal" data-target="#modal_globalstats">Statistiques</a></li>';
                  echo '<li><a class="disabled" href="#" data-toggle="modal" data-target="#modal_backup">Backup</a></li>';
                echo '</ul>';
              echo '</li>';
            }
            echo '<li class="dropdown">';
              echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown">'.$this->user->getPseudo().'<span class="caret"></span></a>';
              echo '<ul class="dropdown-menu" role="menu">';
                echo '<li><a href="#" data-toggle="modal" data-target="#modal_edit_profile">Profil</a></li>';
                echo '<li><a href="#" data-toggle="modal" data-target="#modal_add_translation">Ajouter une traduction</a></li>';
                echo '<li class="divider"></li>';
                echo '<li><a href="actions/logout.php">Déconnexion</a></li>';
              echo '</ul>';
            echo '</li>';
          }
        echo '</ul>';
        echo '</div>';
      echo '</div>';
      echo '</div>';
    }

    /**
    * Render the HTML code required by navbar entries to display required modals
    */
    private function generateModals()
    {
      $modals = array();

      if(!isset($this->user))
      {
        $modals[] = new ModalLogin('modal_login', 'Identification', './actions/login.php');
      }
      else
      {
        $modals[] = new ModalEditProfile('modal_edit_profile', 'Profil', $this->user, './actions/user_save.php');
        $modals[] = new ModalAddTranslation('modal_add_translation', 'Nouvelle traduction', './actions/translation_create.php');

        if($this->user->isAdmin())
        {
          $modals[] = new ModalAddUser('modal_add_user', 'Ajout d\'utilisateur', './actions/user_create.php');
        }
      }

      //Generate created modals
      foreach ($modals as $modal){
        $modal->generate();
      }

      $modals = null;
    }

    public function __destruct()
    {
      unset($this);
    }
  }
?>
