<?php
	class flash {
		private $title;
		private $msg;
		private $isError;

		/**
		*	Default constructor for a flash message.
		* If pIsError is not set, the flash message will be an error.
		*/
		public function __construct($pTitle, $pMsg, $pIsError = true)
		{
			$this->title = $pTitle;
			$this->msg = $pMsg;
			$this->isError = $pIsError;
		}

		/**
		*	Render the HTML code with a Bootstrap alert (alert-success or alert-danger)
		*/
		public function display()
		{
			if($this->isError)
			{
				echo '<div class="alert alert-dismissible alert-danger" role="alert">';
			}
			else
			{
				echo '<div class="alert alert-dismissible alert-success" role="alert">';
			}

			echo '<button type="button" class="close" data-dismiss="alert">';
			echo '<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>';
			echo '</button>';
			echo '<strong>'.$this->title.'</strong>';
			echo '<p>'.$this->msg.'</p>';
			echo '</div>';

			unset($this);
		}

		/**
		*	Destroy the flash, usually called after display.
		*/
		public function __destruct()
		{
			unset($this);
		}
	}
?>
