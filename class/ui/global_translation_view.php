<?php
  require_once dirname(__FILE__).'/../translation.php';
  require_once dirname(__FILE__).'/../status.php';

  class global_translation_view
  {
    private $isIdentifiedUser;

    public function __construct($pIsIdentified = false)
    {
      $this->isIdentifiedUser = $pIsIdentified;
    }

    public function display()
    {
      date_default_timezone_set('Europe/Paris');

      echo '<table class="table table-striped table-hover">';
      echo '<thead>';
        echo '<tr>';
          echo '<th>Traduction</th>';
          echo '<th>Avancement</th>';
          echo '<th>Date de création</th>';
          if($this->isIdentifiedUser)
          {
            echo "<th>Opérations</th>";
          }
        echo '</tr>';
      echo '</thead>';
      echo '<tbody>';

      $translations = Translation::getList();
      foreach ($translations as $translation){
        echo '<tr>';
          echo '<td>';
            $this->generateTitle($translation);
          echo '</td>';
          echo '<td>';
            $this->generateProgressBar($translation);
          echo '</td>';
          echo '<td>'.strftime("%d/%m/%Y", $translation->getCreationDate()).'</td>';
          if($this->isIdentifiedUser)
          {
            echo "<td>TBD</td>";
          }
        echo '</tr>';
      }

      echo '</tbody>';
      echo '</table>';
    }

    /**
    * Generates title with HTML <a> links with special labels
    */
    private function generateTitle($pTranslation)
    {
      //Published
      if($pTranslation->isPublished())
      {
        echo '<a target="_blank" href="'.$pTranslation->getPublicationUrl().'">'.$pTranslation->getTitle().'</a> (<a target="_blank" href="'.$pTranslation->getPadUrl().'">Pad</a>) <span class="label label-success">'.$pTranslation->getStatus()->getStatusName().'</span>';
      }
      else
      {
          //En relecture
          if($pTranslation->isReviewable())
          {
              echo '<a target="_blank" href="'.$pTranslation->getPadUrl().'">'.$pTranslation->getTitle().'</a>  <span class="label label-info">'.$pTranslation->getStatus()->getStatusName().'</span> ';
          }
          elseif($pTranslation->isReviewed())
          {
              echo '<a target="_blank" href="'.$pTranslation->getPadUrl().'">'.$pTranslation->getTitle().'</a>  <span class="label label-warning">'.$pTranslation->getStatus()->getStatusName().'</span> ';
          }
          else
          {
              echo '<a target="_blank" href="'.$pTranslation->getPadUrl().'">'.$pTranslation->getTitle().'</a> ';
          }
      }

      if($pTranslation->isPrior())
      {
          echo ' <span class="label label-danger">Prioritaire</span>';
      }
    }

    /**
    * Generates progress bar for HTML5 compatible browsers or resume string
    */
    private function generateProgressBar($pTranslation)
    {
      $nothtml5 = $pTranslation->getStatus()->getStatusName()." depuis le ".strftime('%d/%m/%Y', $pTranslation->getUpdateDate());
      echo "<progress value='".$pTranslation->getStatus()->getStatusPercentage()."' max='4' title='".$nothtml5."'>[".$nothtml5."]</progress> ";
    }
  }
?>
