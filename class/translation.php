<?php
  require_once dirname(__FILE__).'/db_connection.php';
  require_once dirname(__FILE__).'/../settings.php';

  require_once dirname(__FILE__).'/status.php';

  class Translation {
    private $id;
    private $title;
    private $url_pad;
    private $url_pub;
    private $date_cre;
    private $date_update;
    private $date_end;
    private $is_prior;
    private $status;

    /**
    * Default constructor with everything set to null
    */
    public function __construct()
    {
      $this->id = null;
      $this->title = null;
      $this->url_pad = null;
      $this->url_pub = null;
      $this->date_cre = null;
      $this->date_update = null;
      $this->date_end = null;
      $this->is_prior = null;
      $this->status = null;
    }

    /**
    * Load a translation from its ID
    */
    public static function get($pId)
    {
      $sql = "SELECT count(id_trans) as nb_trad, Title_Trans, UrlPad_Trans, DateCre_Trans, dateupdate_trans, DateEnd_Trans, is_prior, id_status FROM TRANSLATION WHERE id_trans = ".$pId.";";

      $db_conn = new DatabaseConnection();
      $row = $db_conn->doSelect($sql, FALSE);
      $db_conn->closeConnection();

      if($row['nb_trad'] == 1)
      {
        $translation = new Translation();

        $translation->id = $pId;
        $translation->title = $row['title_trans'];
        $translation->url_pad = $row['urlpad_trans'];
        $translation->url_pub = $row['urlpub_trans'];
        $translation->date_cre = $row['datecre_trans'];
        $translation->date_update = $row['dateupdate_trans'];
        $translation->date_end = $row['dateend_trans'];
        $translation->is_prior = $row['is_prior'];
        $translation->status = new status($row['id_status']);

        return $translation;
      }
      return null;
    }

    /**
    * Load multiple translations and returns an array matching with given parameters
    */
    public static function getList($order_param="Title_Trans", $order_by="ASC", $limit=50, $offset=0)
    {
      $sql = "SELECT ID_Trans, Title_Trans, UrlPad_Trans, UrlPub_Trans, DateCre_Trans, dateupdate_trans, DateEnd_Trans, IS_Prior, T.ID_Status, Name_Status FROM TRANSLATION T, STATUS S WHERE S.ID_Status = T.ID_Status ORDER BY ".$order_param." ".$order_by." LIMIT ".$limit." OFFSET ".$offset.";";
      $db_conn = new DatabaseConnection();
      $result = $db_conn->doSelect($sql, TRUE);
      $db_conn->closeConnection();

      $translation_list = array();

      while($row = $result->fetch(PDO::FETCH_ASSOC))
      {
        $translation = new Translation();

        $translation->id = $row['id_trans'];
        $translation->title = $row['title_trans'];
        $translation->url_pad = $row['urlpad_trans'];
        $translation->url_pub = $row['urlpub_trans'];
        $translation->date_cre = $row['datecre_trans'];
        $translation->date_update = $row['dateupdate_trans'];
        $translation->date_end = $row['dateend_trans'];
        $translation->is_prior = $row['is_prior'];
        $translation->status = new status($row['id_status']);

        $translation_list[] = $translation;
      }

      return $translation_list;
    }

    /**
    * Return the title
    */
    public function getTitle()
    {
      return $this->title;
    }

    /**
    * Return the date of creation
    */
    public function getCreationDate()
    {
      return strtotime($this->date_cre);
    }

    /**
    * Return the date of update
    */
    public function getUpdateDate()
    {
      return strtotime($this->date_update);
    }

    /**
    * Return the date of publication
    */
    public function getEndDate()
    {
      return strtotime($this->date_end);
    }

    /**
    * Return the pad URL
    */
    public function getPadUrl()
    {
      return $this->url_pad;
    }

    /**
    * Return the publication URL
    */
    public function getPublicationUrl()
    {
      return $this->url_pub;
    }

    /**
    * Return the boolean value indicating if the translation is prior
    */
    public function isPrior()
    {
      return $this->is_prior;
    }

    /**
    * Return the status object
    */
    public function getStatus()
    {
      return $this->status;
    }

    public function isReviewable()
    {
      if($this->status->getIdStatus() == 1)
      {
        return true;
      }
      return false;
    }

    public function isReviewed()
    {
      if($this->status->getIdStatus() == 2)
      {
        return true;
      }
      return false;
    }

    public function isPublished()
    {
      if($this->status->getIdStatus() == 3)
      {
        return true;
      }
      return false;
    }
  }
?>
