<?php
    require dirname(__FILE__).'/../settings.php';

    class DatabaseConnection {
        private $db_dsn;
        private $db_user;
        private $db_pass;
        private $db_con;

        public function __construct()
        {
            $this->db_dsn = DB_TYPE.':host='.DB_HOST.';dbname='.DB_NAME;
            $this->db_user = DB_USER;
            $this->db_pass = DB_PASSWORD;
            $this->db_con = null;
        }

        /**
        * Open database connection
        */
        private function openConnection()
        {
            try
            {
                $this->db_con = new PDO($this->db_dsn, $this->db_user, $this->db_pass);
                $this->db_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch (PDOException $exc) {
                printf($this->db_dsn."\n");
                die("Database not available : " . $exc->getMessage() );
            }
            return $this->db_con;
        }

        /**
        * Close database connection
        */
        public function closeConnection()
        {
            $this->db_con = null;
        }


        /**
        * Execute a SELECT query.
        * Set hasMultipleResults to TRUE means that there are multiple rows to parsed and returned.
        */
		    public function doSelect($sql, $hasMultipleResults)
        {
          $this->db_con = $this->openConnection();
		      $result = $this->db_con->query($sql);
    			if(!$hasMultipleResults)
    			{
    			     $result = $result->fetch(PDO::FETCH_ASSOC);
    			}

          return $result;
        }

        /**
        * Execute an UPDATE query
        * Returns the number of affected lines
        */
        public function doUpdateOrInsert($sql)
        {
          $this->db_con = $this->openConnection();
          $affected_lines = $this->db_con->exec($sql);
          return $affected_lines;
        }

        /**
        * Execute a DELETE query
        * Returns the number of affected lines
        */
    		public function doDelete($sql)
        {
          $this->db_con = $this->openConnection();
          $affected_lines = $this->db_con->exec($sql);
          return $affected_lines;
        }
    }
?>
